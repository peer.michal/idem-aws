import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_trail(hub, ctx, aws_s3_bucket_policy):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    trail_temp_name = "idem-test-trail-" + str(uuid.uuid4())
    s3_bucket_name = aws_s3_bucket_policy.get("bucket")
    is_multi_region_trail = True
    sns_topic_name = "test-sns-topic"
    tags = {"Name": trail_temp_name}
    is_logging = True
    insight_selectors = [{"InsightType": "ApiCallRateInsight"}]
    event_selectors = [
        {
            "ReadWriteType": "All",
            "IncludeManagementEvents": True,
            "DataResources": [],
            "ExcludeManagementEventSources": [],
        }
    ]

    # Create cloudtrail with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.cloudtrail.trail.present(
        test_ctx,
        name=trail_temp_name,
        s3_bucket_name=s3_bucket_name,
        is_multi_region_trail=is_multi_region_trail,
        sns_topic_name=sns_topic_name,
        tags=tags,
        is_logging=is_logging,
        event_selectors=event_selectors,
        insight_selectors=insight_selectors,
    )

    assert ret["result"], ret["comment"]
    assert f"Would create aws.cloudtrail.trail '{trail_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert s3_bucket_name == resource.get("s3_bucket_name")
    assert is_multi_region_trail == resource.get("is_multi_region_trail")
    assert trail_temp_name == resource.get("name")
    assert tags == resource.get("tags")
    assert sns_topic_name == resource.get("sns_topic_name")
    assert is_logging == resource.get("is_logging")
    assert insight_selectors == resource.get("insight_selectors")
    assert event_selectors == resource.get("event_selectors")

    # Create Cloudtrail in real
    ret = await hub.states.aws.cloudtrail.trail.present(
        ctx,
        name=trail_temp_name,
        s3_bucket_name=s3_bucket_name,
        is_multi_region_trail=is_multi_region_trail,
        tags=tags,
        is_logging=is_logging,
        event_selectors=event_selectors,
        insight_selectors=insight_selectors,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert s3_bucket_name == resource.get("s3_bucket_name")
    assert is_multi_region_trail == resource.get("is_multi_region_trail")
    assert trail_temp_name == resource.get("name")
    assert tags == resource.get("tags")
    resource_id = resource.get("resource_id")
    assert is_logging == resource.get("is_logging")
    assert insight_selectors == resource.get("insight_selectors")
    assert event_selectors == resource.get("event_selectors")

    # Describe CloudTrail
    describe_ret = await hub.states.aws.cloudtrail.trail.describe(
        ctx,
    )
    assert resource_id in describe_ret
    assert "aws.cloudtrail.trail.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.cloudtrail.trail.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "s3_bucket_name" in described_resource_map
    assert s3_bucket_name == described_resource_map["s3_bucket_name"]
    assert is_multi_region_trail == described_resource_map.get("is_multi_region_trail")
    assert tags == described_resource_map.get("tags")
    assert is_logging == described_resource_map.get("is_logging")
    assert insight_selectors == described_resource_map.get("insight_selectors")
    assert event_selectors == described_resource_map.get("event_selectors")

    # Update CloudTrail with test flag
    updated_s3_bucket_name = "sample-bucket"
    is_multi_region_trail = False
    tags.update(
        {
            f"idem-test-cloudtrail-key-{str(uuid.uuid4())}": f"idem-test-cloudtrail-value-{str(uuid.uuid4())}",
        }
    )
    is_logging = False
    event_selectors = [
        {
            "ReadWriteType": "All",
            "IncludeManagementEvents": True,
            "DataResources": [{"Type": "AWS::S3::Object", "Values": ["arn:aws:s3"]}],
            "ExcludeManagementEventSources": [],
        }
    ]
    insight_selectors = [
        {"InsightType": "ApiCallRateInsight"},
        {"InsightType": "ApiErrorRateInsight"},
    ]

    ret = await hub.states.aws.cloudtrail.trail.present(
        test_ctx,
        name=trail_temp_name,
        resource_id=resource_id,
        s3_bucket_name=updated_s3_bucket_name,
        is_multi_region_trail=is_multi_region_trail,
        tags=tags,
        is_logging=is_logging,
        event_selectors=event_selectors,
        insight_selectors=insight_selectors,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert updated_s3_bucket_name == resource.get("s3_bucket_name")
    assert is_multi_region_trail == resource.get("is_multi_region_trail")
    assert tags == resource.get("tags")
    assert is_logging == resource.get("is_logging")
    assert event_selectors == resource.get("event_selectors")
    assert insight_selectors == resource.get("insight_selectors")

    # Update Cloudtrail in real
    ret = await hub.states.aws.cloudtrail.trail.present(
        ctx,
        name=trail_temp_name,
        resource_id=resource_id,
        s3_bucket_name=s3_bucket_name,
        is_multi_region_trail=is_multi_region_trail,
        tags=tags,
        is_logging=is_logging,
        event_selectors=event_selectors,
        insight_selectors=insight_selectors,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")
    assert is_logging == resource.get("is_logging")
    assert event_selectors == resource.get("event_selectors")
    assert insight_selectors == resource.get("insight_selectors")

    # Delete Cloudtrail with test flag
    ret = await hub.states.aws.cloudtrail.trail.absent(
        test_ctx, name=trail_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.cloudtrail.trail", name=trail_temp_name
        )[0]
        in ret["comment"]
    )

    # Delete CloudTrail
    ret = await hub.states.aws.cloudtrail.trail.absent(
        ctx, name=trail_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.cloudtrail.trail", name=trail_temp_name
        )[0]
        in ret["comment"]
    )

    # Delete CloudTrail again
    ret = await hub.states.aws.cloudtrail.trail.absent(
        ctx, name=trail_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.cloudtrail.trail", name=trail_temp_name
        )[0]
        in ret["comment"]
    )
