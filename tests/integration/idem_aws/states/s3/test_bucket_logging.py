import copy
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "test_bucket_name",
    "bucket_logging_status": {
        "LoggingEnabled": {
            "TargetBucket": "test_bucket_name",
            "TargetPrefix": "log",
            "TargetGrants": [
                {
                    "Grantee": {
                        "URI": "http://acs.amazonaws.com/groups/s3/LogDelivery",
                        "Type": "Group",
                    },
                    "Permission": "WRITE",
                },
                {
                    "Grantee": {
                        "URI": "http://acs.amazonaws.com/groups/s3/LogDelivery",
                        "Type": "Group",
                    },
                    "Permission": "READ",
                },
            ],
        }
    },
}


@pytest.fixture(scope="module")
async def aws_s3_bucket_for_logging(hub, ctx, aws_s3_bucket_for_acl):
    # Grant S3 Log Delivery group write access to make PutBucketLogging action successful.
    result = await hub.exec.boto3.client.s3.get_bucket_acl(
        ctx, Bucket=aws_s3_bucket_for_acl.get("name")
    )

    access_control_policy = {
        "Owner": result["ret"]["Owner"],
        "Grants": [
            {
                "Grantee": {
                    "Type": "Group",
                    "URI": "http://acs.amazonaws.com/groups/s3/LogDelivery",
                },
                "Permission": "WRITE",
            },
            {
                "Grantee": {
                    "Type": "Group",
                    "URI": "http://acs.amazonaws.com/groups/s3/LogDelivery",
                },
                "Permission": "READ_ACP",
            },
        ],
    }

    ret = await hub.states.aws.s3.bucket_acl.present(
        ctx,
        name=aws_s3_bucket_for_acl.get("name"),
        access_control_policy=access_control_policy,
    )

    # It should be successful
    assert ret["result"], ret["comment"]

    yield aws_s3_bucket_for_acl


@pytest.mark.asyncio
@pytest.mark.localstack(Pro=False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, aws_s3_bucket_for_logging, __test):
    global PARAMETER
    bucket = aws_s3_bucket_for_logging.get("name")

    PARAMETER["bucket_logging_status"]["LoggingEnabled"]["TargetBucket"] = bucket
    PARAMETER["name"] = bucket
    ctx["test"] = __test

    ret = await hub.states.aws.s3.bucket_logging.present(
        ctx,
        **PARAMETER,
    )

    assert ret["result"], ret["comment"]

    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.s3.bucket_logging",
                name=PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.s3.bucket_logging",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert_bucket_logging(hub, ctx, resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.s3.bucket_logging.describe(ctx)
    # Verify that describe output format is correct
    assert "aws.s3.bucket_logging.present" in describe_ret.get(
        PARAMETER["name"] + "-logging"
    )
    describe_params = describe_ret.get(PARAMETER["name"] + "-logging").get(
        "aws.s3.bucket_logging.present"
    )
    described_resource_map = dict(ChainMap(*describe_params))
    assert_bucket_logging(hub, ctx, described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="modify_bucket_logging_attributes", depends=["present"])
async def test_modify_attributes(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test

    new_parameter["bucket_logging_status"]["LoggingEnabled"][
        "TargetPrefix"
    ] = "test-log"

    ret = await hub.states.aws.s3.bucket_logging.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.s3.bucket_logging",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.s3.bucket_logging",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert_bucket_logging(hub, ctx, ret["old_state"], PARAMETER)
    assert_bucket_logging(hub, ctx, ret["new_state"], new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="test_exec_get", depends=["modify_bucket_logging_attributes"]
)
async def test_exec_get(hub, ctx, __test):
    global PARAMETER
    ret = await hub.exec.aws.s3.bucket_logging.get(
        ctx=ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert_bucket_logging(hub, ctx, ret["ret"], PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["test_exec_get"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.s3.bucket_logging.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert_bucket_logging(hub, ctx, ret["old_state"], PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.s3.bucket_logging",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.s3.bucket_logging",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.s3.bucket_logging.absent(
        ctx,
        name=PARAMETER["name"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.s3.bucket_logging",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )


def assert_bucket_logging(hub, ctx, resource, parameters):
    assert parameters.get("name") == resource.get("name")
    assert parameters.get("bucket_logging_status") == resource.get(
        "bucket_logging_status"
    )
    assert parameters.get("name") == resource.get("resource_id")
