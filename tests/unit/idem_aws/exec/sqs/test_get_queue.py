from asyncio import Future
from unittest.mock import MagicMock

PRESENT_ATTRIBUTES = {
    "delay_seconds": 55,
    "maximum_message_size": 5321,
    "message_retention_period": 175,
    "policy": {
        "Version": "2012-10-17",
        "Id": "Test_Queue_Policy",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {"AWS": ["111111111111"]},
                "Action": "sqs:SendMessage",
                "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo",
            }
        ],
    },
    "receive_message_wait_time_seconds": 5,
    "redrive_policy": {
        "deadLetterTargetArn": "aws:sqs:queue:arn",
        "maxReceiveCount": 3,
    },
    "visibility_timeout": 1579,
    "kms_master_key_id": "sample/aws/sqs",
    "kms_data_key_reuse_period_seconds": 125,
    "sqs_managed_sse_enabled": False,
    "fifo_queue": True,
    "content_based_deduplication": True,
    "deduplication_scope": "message_group",
    "fifo_throughput_limit": "perMessageGroupId",
}

RAW_ATTRIBUTES = {
    "DelaySeconds": "55",
    "MaximumMessageSize": "5321",
    "MessageRetentionPeriod": "175",
    "Policy": '{"Version": "2012-10-17", "Id": "Test_Queue_Policy", "Statement": '
    '[{"Effect": "Allow", "Principal": {"AWS": ["111111111111"]}, "Action": '
    '"sqs:SendMessage", "Resource": "arn:aws:sqs:us-west-1:111111111111:sqs_queue.fifo"}]}',
    "ReceiveMessageWaitTimeSeconds": "5",
    "RedrivePolicy": '{"deadLetterTargetArn": "aws:sqs:queue:arn", '
    '"maxReceiveCount": 3}',
    "VisibilityTimeout": "1579",
    "KmsMasterKeyId": "sample/aws/sqs",
    "KmsDataKeyReusePeriodSeconds": "125",
    "SqsManagedSseEnabled": "False",
    "FifoQueue": "True",
    "ContentBasedDeduplication": "True",
    "DeduplicationScope": "message_group",
    "FifoThroughputLimit": "perMessageGroupId",
}


async def test_get_queue(hub, mock_hub, ctx):
    # Set up the mock hub
    mock_hub.exec.boto3.client.sqs = hub.exec.boto3.client.sqs
    mock_hub.exec.aws.sqs.queue.get = hub.exec.aws.sqs.queue.get
    mock_hub.tool.aws.sqs.conversion_utils.convert_raw_attributes_to_present = (
        hub.tool.aws.sqs.conversion_utils.convert_raw_attributes_to_present
    )

    # Mocked async client.sqs.get_queue_attributes boto3 call
    mock_get_queue_attributes_future = Future()
    mock_get_queue_attributes_future.set_result(
        dict(result=True, ret={"Attributes": RAW_ATTRIBUTES}, comment=[])
    )
    mock_hub.exec.boto3.client.sqs.get_queue_attributes = MagicMock(
        return_value=mock_get_queue_attributes_future
    )

    # Mocked async client.sqs.list_queue_tags boto3 call
    mock_list_queue_tags_future = Future()
    tags = {"Sample": "abc", "TYPE": "something", "model": "123"}
    mock_list_queue_tags_future.set_result(
        dict(result=True, ret={"Tags": tags}, comment=[])
    )
    mock_hub.exec.boto3.client.sqs.list_queue_tags = MagicMock(
        return_value=mock_list_queue_tags_future
    )

    name = "sqs_queue.fifo"
    resource_id = f"http://example.com:4566/111111111111/{name}"
    result = await mock_hub.exec.aws.sqs.queue.get(ctx, resource_id)

    # Check the mocked boto3 calls are called with the correct arguments in the tested method
    mock_hub.exec.boto3.client.sqs.get_queue_attributes.assert_called_with(
        ctx, QueueUrl=resource_id, AttributeNames=["All"]
    )
    mock_hub.exec.boto3.client.sqs.get_queue_tags.assert_called_with(
        ctx, QueueUrl=resource_id
    )

    expected_queue_ret = {
        "name": name,
        "resource_id": resource_id,
        **PRESENT_ATTRIBUTES,
        "tags": tags,
    }

    assert result["result"], result["comment"]
    assert result.get("ret") == expected_queue_ret


async def test_get_non_existent_queue(hub, mock_hub, ctx):
    # Set up the mock hub
    mock_hub.exec.boto3.client.sqs = hub.exec.boto3.client.sqs
    mock_hub.exec.aws.sqs.queue.get = hub.exec.aws.sqs.queue.get
    mock_hub.tool.aws.sqs.conversion_utils.convert_raw_attributes_to_present = (
        hub.tool.aws.sqs.conversion_utils.convert_raw_attributes_to_present
    )
    mock_hub.tool.aws.comment_utils.get_empty_comment = (
        hub.tool.aws.comment_utils.get_empty_comment
    )

    # Mocked async client.sqs.get_queue_attributes boto3 call
    mock_queue_does_not_exist_future = Future()
    mock_queue_does_not_exist_future.set_result(
        dict(result=False, ret=None, comment=("QueueDoesNotExist",))
    )
    mock_hub.exec.boto3.client.sqs.get_queue_attributes = MagicMock(
        return_value=mock_queue_does_not_exist_future
    )

    # Mocked async client.sqs.list_queue_tags boto3 call
    mock_hub.exec.boto3.client.sqs.list_queue_tags = MagicMock(
        return_value=mock_queue_does_not_exist_future
    )

    name = "sqs_queue.fifo"
    resource_id = f"http://example.com:4566/111111111111/{name}"
    result = await mock_hub.exec.aws.sqs.queue.get(ctx, resource_id)

    # Check the mocked boto3 calls are called with the correct arguments in the tested method
    mock_hub.exec.boto3.client.sqs.get_queue_attributes.assert_called_with(
        ctx, QueueUrl=resource_id, AttributeNames=["All"]
    )
    mock_hub.exec.boto3.client.sqs.get_queue_tags.assert_called_with(
        ctx, QueueUrl=resource_id
    )

    assert result["result"], result["comment"]
    assert (
        hub.tool.aws.comment_utils.get_empty_comment(
            resource_type="aws.sqs.queue", name=name
        )
        in result["comment"]
    )
    assert result.get("ret") is None


async def test_get_queue_with_waiter(hub, mock_hub, ctx):
    # Set up the mock hub
    mock_hub.exec.boto3.client.sqs = hub.exec.boto3.client.sqs
    mock_hub.exec.aws.sqs.queue.get = hub.exec.aws.sqs.queue.get
    mock_hub.tool.aws.sqs.conversion_utils.convert_raw_attributes_to_present = (
        hub.tool.aws.sqs.conversion_utils.convert_raw_attributes_to_present
    )

    # Mocked async client.sqs.get_queue_attributes boto3 call
    mock_get_queue_attributes_future = Future()
    mock_get_queue_attributes_future.set_result(
        dict(result=True, ret={"Attributes": RAW_ATTRIBUTES}, comment=[])
    )
    mock_hub.exec.boto3.client.sqs.get_queue_attributes = MagicMock(
        return_value=mock_get_queue_attributes_future
    )

    # Mocked async client.sqs.list_queue_tags boto3 call
    mock_list_queue_tags_future = Future()
    tags = {"Sample": "abc", "TYPE": "something", "model": "123"}
    mock_list_queue_tags_future.set_result(
        dict(result=True, ret={"Tags": tags}, comment=[])
    )
    mock_hub.exec.boto3.client.sqs.list_queue_tags = MagicMock(
        return_value=mock_list_queue_tags_future
    )

    name = "sqs_queue.fifo"
    resource_id = f"http://example.com:4566/111111111111/{name}"
    result = await mock_hub.exec.aws.sqs.queue.get(
        ctx, resource_id, expected_attributes=RAW_ATTRIBUTES, expected_tags=tags
    )

    # Check the mocked boto3 calls are called with the correct arguments in the tested method
    mock_hub.exec.boto3.client.sqs.get_queue_attributes.assert_called_with(
        ctx, QueueUrl=resource_id, AttributeNames=["All"]
    )
    mock_hub.exec.boto3.client.sqs.get_queue_tags.assert_called_with(
        ctx, QueueUrl=resource_id
    )

    expected_queue_ret = {
        "name": name,
        "resource_id": resource_id,
        **PRESENT_ATTRIBUTES,
        "tags": tags,
    }

    assert result["result"], result["comment"]
    assert result.get("ret") == expected_queue_ret
