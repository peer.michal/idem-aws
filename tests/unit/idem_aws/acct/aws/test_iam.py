import configparser
from unittest import mock

import dict_tools.data
import pytest

PROFILE = {
    "aws.iam": {
        "paths": ["./path"],
        "region": "us-east-1",
    }
}


class MockConfigParser(configparser.ConfigParser):
    def read(self, filenames, *args, **kwargs):
        self.read_string(
            "[default]\naws_access_key_id = my_key\naws_secret_access_key = my_secret_key"
        )


@pytest.mark.asyncio
async def test_gather(hub):
    hub.OPT = dict_tools.data.NamespaceDict(acct=dict(extras=PROFILE))

    with mock.patch.object(configparser, "ConfigParser", MockConfigParser):
        p1 = await hub.acct.init.gather(
            subs=["aws"], profile="default", profiles=PROFILE
        )

    assert p1["aws_access_key_id"] == "my_key"
    assert p1["aws_secret_access_key"] == "my_secret_key"
    assert p1["region_name"] == "us-east-1"
    assert "paths" not in p1


@pytest.mark.asyncio
async def test_gather_region_name_from_extras(hub):
    """
    Verify that region_name missing from the credentials profile is read from config
    """
    # region_name in 'extras'
    hub.OPT = dict_tools.data.NamespaceDict(
        {"acct": {"extras": {"aws": {"region_name": "config-region"}}}}
    )

    with mock.patch.object(configparser, "ConfigParser", MockConfigParser):
        p1 = await hub.acct.init.gather(subs=["aws"], profile="default", profiles={})

    assert p1["region_name"] == "config-region"
    assert p1["aws_access_key_id"] == "my_key"
    assert p1["aws_secret_access_key"] == "my_secret_key"
