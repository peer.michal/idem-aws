"""State module for managing Amazon Mount Targets."""
import copy
from typing import Any
from typing import Dict
from typing import List

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    file_system_id: str,
    subnet_id: str,
    resource_id: str = None,
    ip_address: str = None,
    security_groups: List[str] = None,
) -> Dict[str, Any]:
    """Creates a mount target.

    Creates a mount target for a file system. You can then mount the file system on EC2 instances by using the mount
    target. You can create one mount target in each Availability Zone in your VPC. All EC2 instances in a VPC within
    a given Availability Zone share a single mount target for a given file system. If you have multiple subnets in
    an Availability Zone, you create a mount target in one of the subnets. EC2 instances do not need to be in the
    same subnet as the mount target in order to access their file system. You can create only one mount target for
    an EFS file system using One Zone storage classes. You must create that mount target in the same Availability
    Zone in which the file system is located. Use the AvailabilityZoneName and AvailabiltyZoneId properties in the
    DescribeFileSystems response object to get this information. Use the subnetId associated with the file system's
    Availability Zone when creating the mount target. For more information, see Amazon EFS: How it Works.  To create
    a mount target for a file system, the file system's lifecycle state must be available. For more information, see
    DescribeFileSystems.

    In the request, provide the following:
        * The file system ID for which you are creating the mount target.
        * A subnet ID, which determines the following:
            * The VPC in which Amazon EFS creates the mount target
            * The Availability Zone in which Amazon EFS creates the mount target
            * The IP address range from which Amazon EFS selects the IP address of the mount target
              (if you don't specify an IP address in the request)

    After creating the mount target, Amazon EFS returns a response that includes, a MountTargetId and an IpAddress.
    You use this IP address when mounting the file system in an EC2 instance. You can also use the mount target's
    DNS name when mounting the file system. The EC2 instance on which you mount the file system by using the mount
    target can resolve the mount target's DNS name to its IP address. For more information, see How it Works:
    Implementation Overview.  Note that you can create mount targets for a file system in only one VPC, and there
    can be only one mount target per Availability Zone. That is, if the file system already has one or more mount
    targets created for it, the subnet specified in the request to add another mount target must meet the following
    requirements:
        * Must belong to the same VPC as the subnets of the existing mount targets
        * Must not be in the same Availability Zone as any of the subnets of the existing mount targets

    If the request satisfies the requirements, Amazon EFS does the following:
        * Creates a new mount target in the specified subnet.
        * Also  creates a new network interface in the subnet as follows:
            * If the request provides an IpAddress, Amazon EFS assigns that IP address to the network interface.
              Otherwise, Amazon EFS assigns a free address in the subnet (in the same way that the Amazon EC2
              CreateNetworkInterface call does when a request does not specify a primary private IP address).
            * If the request provides SecurityGroups, this network interface is associated with those security groups.
              Otherwise, it belongs to the default security group for the subnet's VPC.
            * Assigns the description
              ``Mount target fsmt-id for file system fs-id  where  fsmt-id  is the mount target ID, and  fs-id  is
              the FileSystemId.``
            * Sets the requesterManaged property of the network interface to true, and the requesterId
              value to EFS.
    Each Amazon EFS mount target has one corresponding requester-managed EC2 network interface.
    After the network interface is created, Amazon EFS sets the NetworkInterfaceId field in the mount target's
    description to the network interface ID, and the IpAddress field to its address. If network interface creation
    fails, the entire CreateMountTarget operation fails.

    .. Note::
        The CreateMountTarget call returns only after creating
        the network interface, but while the mount target state is still creating, you can check the mount target
        creation status by calling the DescribeMountTargets operation, which among other things returns the mount target
        state.

    We recommend that you create a mount target in each of the Availability Zones. There are cost
    considerations for using a file system in an Availability Zone through a mount target created in another
    Availability Zone. For more information, see Amazon EFS. In addition, by always using a mount target local to
    the instance's Availability Zone, you eliminate a partial failure scenario. If the Availability Zone in which
    your mount target is created goes down, then you can't access your file system through that mount target.

    This operation requires permissions for the following action on the file system:
        * elasticfilesystem:CreateMountTarget

    This operation also requires permissions for the following Amazon EC2 actions:
        * ec2:DescribeSubnets
        * ec2:DescribeNetworkInterfaces
        * ec2:CreateNetworkInterface

    Args:
        name(str):
            An Idem name of the resource.
        file_system_id(str):
            The ID of the file system for which to create the mount target.
        subnet_id(str):
            The ID of the subnet to add the mount target in. For file systems that use One Zone storage
            classes, use the subnet that is associated with the file system's Availability Zone.
        ip_address(str, Optional):
            Valid IPv4 address within the address range of the specified subnet. Defaults to None.
        security_groups(List[str], Optional):
            Up to five VPC security group IDs, of the form sg-xxxxxxxx. These must be for the same VPC as
            subnet specified. Defaults to None.
        resource_id(str, Optional):
            The ID of mount target. Defaults to None.

    Request Syntax:
        .. code-block:: sls

            [ mount-target-resource-id ]:
                aws.efs.mount_target.present:
                  - name: "string"
                  - resource_id: "string"
                  - security_groups: List
                  - file_system_id: "string"
                  - subnet_id: "string"
                  - ip_address: "string"

    Returns:
        Dict[str, Any]

    Examples:
        .. code-block:: sls

            fsmt-xxxxxxxxxxx:
              aws.efs.mount_target.present:
                - name: fsmt-xxxxxxxxxxx
                - resource_id: fsmt-xxxxxxxxxxx
                - security_groups:
                    - sg-xxxxxxxxx
                    - sg-xxxxxxxxx
                - file_system_id: fs-xxxxxxxxxxxxxxx
                - subnet_id: subnet-xxxxxxxxxx
                - ip_address: 127.0.0.1
    """
    result = dict(comment=[], old_state=None, new_state=None, name=name, result=True)

    # Query for mount target
    mount_target = None
    if resource_id:
        mount_target = await hub.exec.boto3.client.efs.describe_mount_targets(
            ctx, MountTargetId=resource_id
        )
        if not mount_target["result"]:
            result["comment"] = mount_target["comment"]
            result["result"] = mount_target["result"]
            return result

    # Check for existing mount target
    before = None
    if mount_target and mount_target["ret"]["MountTargets"]:
        before = mount_target["ret"]["MountTargets"][0]

    # Check for security groups of mount target
    mount_target_security_groups = []
    if before:
        security_groups_result = (
            await hub.exec.boto3.client.efs.describe_mount_target_security_groups(
                ctx, MountTargetId=resource_id
            )
        )
        if not security_groups_result["result"]:
            hub.log.warning(
                f"Could not describe security groups for mount target {resource_id} : {security_groups_result['comment']}"
            )
            result["comment"] += [security_groups_result["comment"]]
            result["result"] = False
            return result

        mount_target_security_groups = security_groups_result["ret"]["SecurityGroups"]
    # Update current state
    current_state = (
        hub.tool.aws.efs.mount_target_utils.convert_raw_mount_target_to_present(
            ctx=ctx,
            mount_target=before,
            security_groups=mount_target_security_groups,
        )
    )
    result["old_state"] = current_state

    # Handle No change behaviour
    desired_state = {
        "name": name,
        "resource_id": resource_id,
        "file_system_id": file_system_id,
        "subnet_id": subnet_id,
        "ip_address": ip_address,
        "security_groups": security_groups,
    }

    is_security_groups_updated = not current_state or (
        not hub.tool.aws.state_comparison_utils.are_lists_identical(
            current_state.get("security_groups"), desired_state.get("security_groups")
        )
    )

    is_change_detected = before is None or is_security_groups_updated

    if not is_change_detected:
        result["comment"] += [f"aws.efs.mount_target '{name}' already exists"]
        result["new_state"] = copy.deepcopy(result["old_state"])
        return result

    # Handle test behaviour
    if ctx.get("test", False):
        result["new_state"] = hub.tool.aws.test_state_utils.generate_test_state(
            enforced_state=current_state,
            desired_state=desired_state,
        )
        result["comment"] = (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.efs.mount_target", name=name
            )
            if before
            else hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.efs.mount_target", name=name
            )
        )
        return result

    # Handle actual resource create or update
    if before:
        ret = await hub.exec.boto3.client.efs.modify_mount_target_security_groups(
            ctx, MountTargetId=resource_id, SecurityGroups=security_groups
        )
        result["result"] = ret["result"]
        if not result["result"]:
            result["comment"] = ret["comment"]
            return result
        result["comment"] += [
            f"Updated security groups for aws.efs.mount_target '{name}'"
        ]
    else:
        ret = await hub.exec.boto3.client.efs.create_mount_target(
            ctx,
            **{
                "FileSystemId": file_system_id,
                "SubnetId": subnet_id,
                "IpAddress": ip_address,
                "SecurityGroups": security_groups,
            },
        )
        result["result"] = ret["result"]
        if not result["result"]:
            result["comment"] = ret["comment"]
            return result
        result["comment"] = hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.efs.mount_target", name=name
        )
        resource_id = ret["ret"]["MountTargetId"]

    # Fetch the updated resource and update new_state
    mount_target = await hub.exec.boto3.client.efs.describe_mount_targets(
        ctx, MountTargetId=resource_id
    )
    if not mount_target["result"]:
        result["comment"] += mount_target["comment"]
        result["result"] = mount_target["result"]
        return result

    after = mount_target["ret"]["MountTargets"][0]
    security_groups_result = (
        await hub.exec.boto3.client.efs.describe_mount_target_security_groups(
            ctx, MountTargetId=resource_id
        )
    )
    mount_target_security_groups = security_groups_result["ret"]["SecurityGroups"]
    result[
        "new_state"
    ] = hub.tool.aws.efs.mount_target_utils.convert_raw_mount_target_to_present(
        ctx=ctx,
        mount_target=after,
        security_groups=mount_target_security_groups,
    )

    return result


async def absent(
    hub,
    ctx,
    name: str,
    resource_id: str = None,
    timeout: Dict = None,
) -> Dict[str, Any]:
    """Deletes the specified mount target.

    This operation forcibly breaks any mounts of the file system by using the
    mount target that is being deleted, which might disrupt instances or applications using those mounts. To avoid
    applications getting cut off abruptly, you might consider unmounting any mounts of the mount target, if
    feasible. The operation also deletes the associated network interface. Uncommitted writes might be lost, but
    breaking a mount target using this operation does not corrupt the file system itself. The file system you
    created remains. You can mount an EC2 instance in your VPC by using another mount target.

    This operation requires permissions for the following action on the file system:
        * ``elasticfilesystem:DeleteMountTarget``

    .. Note::
        The ``DeleteMountTarget`` call returns while the mount target state is still deleting. You can check the mount target
        deletion by calling the DescribeMountTargets operation, which returns a list of mount target descriptions for
        the given file system.

    The operation also requires permissions for the following Amazon EC2 action on the mount target's network interface:
        * ``ec2:DeleteNetworkInterface``

    Args:
        name(str):
            An Idem name of the resource.
        resource_id(str, Optional):
            The ID of mount target.
        timeout(dict, Optional):
            Timeout configuration for deletion of AWS EFS mount target.
                * delete (dict) -- Timeout configuration for deletion of an EFS mount target
                * delay -- The amount of time in seconds to wait between attempts. Defaults to 15
                * max_attempts -- Customized timeout configuration containing delay and max attempts. Defaults to 40

    Request Syntax:
        .. code-block:: sls

           [mount-target-resource-id]:
                aws.efs.mount_target.absent:
                  - name: "string"
                  - resource_id: "string"
    Returns:
        Dict[str, Any]

    Examples:
        .. code-block:: sls

            fsmt-xxxxxxxxxxx:
              aws.efs.mount_target.absent:
                - name: fsmt-xxxxxxxxxxx
                - resource_id: fsmt-xxxxxxxxxxx
    """
    result = dict(comment=[], old_state=None, new_state=None, name=name, result=True)

    # Query for mount target
    mount_target = None
    if resource_id:
        mount_target = await hub.exec.boto3.client.efs.describe_mount_targets(
            ctx, MountTargetId=resource_id
        )

    # Check for existing mount target
    before = None
    if mount_target and mount_target["result"] and mount_target["ret"]["MountTargets"]:
        before = mount_target["ret"]["MountTargets"][0]

    if before:
        # Check for mount target security group
        mount_target_security_groups = []
        security_groups_result = (
            await hub.exec.boto3.client.efs.describe_mount_target_security_groups(
                ctx, MountTargetId=resource_id
            )
        )
        if security_groups_result["result"]:
            mount_target_security_groups = security_groups_result["ret"][
                "SecurityGroups"
            ]
        else:
            hub.log.warning(
                f"Could not describe security groups for mount target {resource_id} : {security_groups_result['comment']}"
            )
        result[
            "old_state"
        ] = hub.tool.aws.efs.mount_target_utils.convert_raw_mount_target_to_present(
            ctx=ctx,
            mount_target=before,
            security_groups=mount_target_security_groups,
        )

    if not before:
        result["comment"] = hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.efs.mount_target", name=name
        )
    elif ctx.get("test", False):
        result["comment"] = hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.efs.mount_target", name=name
        )
    else:
        ret = await hub.exec.boto3.client.efs.delete_mount_target(
            ctx, MountTargetId=resource_id
        )
        if not ret["result"]:
            result["result"] = ret["result"]
            result["comment"] = ret["comment"]
            return result

        # Custom waiter for delete
        waiter_config = hub.tool.aws.waiter_utils.create_waiter_config(
            default_delay=15,
            default_max_attempts=40,
            timeout_config=timeout.get("delete") if timeout else None,
        )
        delete_waiter_acceptors = [
            {
                "matcher": "error",
                "expected": "MountTargetNotFound",
                "state": "success",
                "argument": "Error.Code",
            },
            {
                "matcher": "pathAll",
                "expected": "error",
                "state": "success",
                "argument": "MountTargets[].LifeCycleState",
            },
            {
                "matcher": "pathAll",
                "expected": "available",
                "state": "retry",
                "argument": "MountTargets[].LifeCycleState",
            },
            {
                "matcher": "pathAll",
                "expected": "deleting",
                "state": "retry",
                "argument": "MountTargets[].LifeCycleState",
            },
            {
                "matcher": "pathAll",
                "expected": "deleted",
                "state": "success",
                "argument": "MountTargets[].LifeCycleState",
            },
        ]

        cluster_waiter = hub.tool.boto3.custom_waiter.waiter_wrapper(
            name="MountTargetDelete",
            operation="DescribeMountTargets",
            argument=["Error.Code", "MountTargets[].LifeCycleState"],
            acceptors=delete_waiter_acceptors,
            client=await hub.tool.boto3.client.get_client(ctx, "efs"),
        )
        try:
            await hub.tool.boto3.client.wait(
                ctx,
                "efs",
                "MountTargetDelete",
                cluster_waiter,
                MountTargetId=resource_id,
                WaiterConfig=waiter_config,
            )
        except Exception as e:
            result["comment"] += [str(e)]
            result["result"] = False
            return result

        result["comment"] = hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.efs.mount_target", name=name
        )

    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """Describe the resource in a way that can be recreated/managed with the corresponding "present" function.

    Returns the descriptions of all the current mount targets, or a specific mount target, for a file system. When
    requesting all of the current mount targets, the order of mount targets returned in the response is unspecified.

    This operation requires permissions for the ``elasticfilesystem:DescribeMountTargets`` action, on either the file
    system ID that you specify in ``FileSystemId``, or on the file system of the mount target that you specify in
    ``MountTargetId``.

    Returns:
        Dict[str, Any]

    Examples:
        .. code-block:: bash

            $ idem describe aws.efs.mount_target
    """
    result = {}

    # Fetch all file system ids
    file_system_ids = []
    file_systems = await hub.exec.boto3.client.efs.describe_file_systems(ctx)
    if not file_systems["result"]:
        hub.log.warning(f"Could not describe file_system {file_systems['comment']}")
        return result
    for file_system in file_systems["ret"]["FileSystems"]:
        file_system_ids.append(file_system["FileSystemId"])

    # Fetch all mount targets
    mount_targets = []
    for file_system_id in file_system_ids:
        mount_targets_result = await hub.exec.boto3.client.efs.describe_mount_targets(
            ctx, FileSystemId=file_system_id
        )
        if not mount_targets_result["result"]:
            hub.log.warning(
                f"Could not describe mount_target for file system {file_system_id} : {mount_targets_result['comment']}"
            )
            continue
        mount_targets.extend(mount_targets_result["ret"]["MountTargets"])

    # Describe each mount target
    for mount_target in mount_targets:
        mount_target_id = mount_target["MountTargetId"]

        security_groups_result = (
            await hub.exec.boto3.client.efs.describe_mount_target_security_groups(
                ctx, MountTargetId=mount_target_id
            )
        )
        if not security_groups_result["result"]:
            hub.log.warning(
                f"Could not describe security groups for mount target {mount_target_id} : {security_groups_result['comment']}"
            )
            continue

        security_groups = security_groups_result["ret"]["SecurityGroups"]
        resource = (
            hub.tool.aws.efs.mount_target_utils.convert_raw_mount_target_to_present(
                ctx=ctx,
                mount_target=mount_target,
                security_groups=security_groups,
            )
        )
        resource_id = resource["resource_id"]
        result[resource_id] = {
            "aws.efs.mount_target.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result
